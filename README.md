# Raspberry Temperature

This project allows the monitoring of the temperature of my raspberry pi

A python script running on my raspberry is collecting the temperature every hour. This project uses this data and displays graphs

Technologies used : React-Js, Chart-Js, react-chartjs-2, Python (to create the json file which contains the data to process)

![alt text](https://raw.githubusercontent.com/Nairolf92/raspberry-temperature/master/temperature-11-11-2018.png)

## CI

* Projet hébergé sur [Gitlab](https://gitlab.com/Nairolf92/raspberry-temperature)
* Un stage de build commun à la branche dev et master
* Un stage de déploiement sur une pré-production quand push sur la branche dev
* Un stage de déploiement sur une production quand push sur la branche master


## CD


* Le push sur la branche dev déploit un build en pré-production sur Hiroku : [Pré-production Hiroku](https://raspberry-temperature-preprod.herokuapp.com/raspberry-temperature)
* Le push sur la branche master déploit un build en production sur Hiroku : [Production Hiroku](https://raspberry-temperature.herokuapp.com/raspberry-temperature)

